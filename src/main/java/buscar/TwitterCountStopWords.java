package buscar;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class TwitterCountStopWords {
	public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Usage: TwitterCountStopWords <input path> <output path>");
            System.exit(-1);
        }
        Job job = new Job();
        job.setJarByClass(buscar.TwitterCountStopWords.class);
        job.setJobName("Count DiferentStopWords");

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        //job.setMapperClass(buscar.MapperPhase.class);
        job.setMapperClass(Mapper.MapperCountStopWords.class);
        job.setReducerClass(Reducer.ReducerCountStopWords.class);
        //job.setCombinerClass(edu.uprm.cse.bigdata.mrsp02.TwitterReduceByScreenName.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        //-------------guardar resultado en file
//        if(job.waitForCompletion(true)) 
//        {
//        	Path path = new Path(args[1]+"/part-r-00000");
//        	FileSystem fs =FileSystem.get(path.toUri(),job.getConfiguration());
//        }
        System.out.println("Done!");
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
