package Reducer;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class ReducerCountMessage extends Reducer<Text, Text, Text, Text> {
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException{
    	
    	java.lang.StringBuilder lista= new java.lang.StringBuilder();
    	int c=0;
        for (Text value : values){
        	lista.append(","+ value);
            c++;
        }
        //--------------------- muestra el mensaje y la cantidad de posts
        
        context.write(key, new Text("["+c+"]"+lista.toString()));
       
    }
}
