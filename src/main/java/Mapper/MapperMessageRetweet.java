package Mapper;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;

public class MapperMessageRetweet extends Mapper<LongWritable, Text, Text, Text> {
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException{
        String rawTweet = value.toString();
        try{
            Status status = TwitterObjectFactory.createStatus(rawTweet);
            if (status.isRetweet()) {
            	//-------------- recupera retweet y id -------------
                long tweetID = status.getRetweetedStatus().getId();
                
                context.write(new Text(String.valueOf(tweetID)),new Text(Long.toString(status.getId())));
                
            }
            //context.write(new Text("luciaaaaaa"),new Text("uuuuuuuu"));

        }catch (TwitterException e) {}
    }
}
