package Mapper;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;

public class KeywordToTweetsMapper extends Mapper<LongWritable, Text, Text, Text> {
    @Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException{
    	String rawTweet = value.toString();
        try {
            Status status = TwitterObjectFactory.createStatus(rawTweet);
            String tweet = status.getText().toUpperCase();
            Long id= status.getId();

            if (tweet.contains("FLU")){
                context.write(new Text("FLU"), new Text(id.toString()));
            }
            if (tweet.contains("ZIKA")){
                context.write(new Text("ZIKA"), new Text(id.toString()));
            }
            if (tweet.contains("DIARRHEA")){
                context.write(new Text("DIARRHEA"), new Text(id.toString()));
            }
            if (tweet.contains("EBOLA")){
                context.write(new Text("EBOLA"), new Text(id.toString()));
            }
            if (tweet.contains("SWAMP")){
                context.write(new Text("SWAMP"), new Text(id.toString()));
            }
            if (tweet.contains("CHANGE")){
                context.write(new Text("CHANGE"), new Text(id.toString()));
            }
        }
        catch(TwitterException e){

        }

    }
}
